<?
/* ამ ფაილში ხდება ცვლადების დეკლარირება(დანიშნულება 1). ანუ $_GET ცვლადები გადაყავს $variable ცვლადებში, რათა გლობალური ცვლადების გამოყენება ავიცილოთ თავიდან. 
ეტაპობრივად ამ ფაილში მოხდება ცვლადების განაწილება(დანიშნულება 2), თუ .htaccess-ის შემდეგ დარჩა ამის აუცილებლობა. ანუ შექმნის ახალ, ისეთი ტიპის ცვლადებს, როგორიცაა ვთქვათ $page_num, რომელიც არ გადაეცემა Query String–ით. 
*/


/*************************************************************************
//should we remove this??
*/


if ($_POST) {
    foreach (array_keys($_POST) as $var) {
        //${$var} = quote_smart_post(${$var});
		$_POST[$var] = quote_smart_post($_POST[$var]);
    }
}
if ($_GET) {
    foreach (array_keys($_GET) as $var) {
        	//${$var} = quote_smart_get(${$var});
			$_GET[$var] = quote_smart_post($_GET[$var]);
    }
}
//should we remove this??
/////////////////////////////////////////////////////////////////////////


if(isset($_SESSION['lang'])){
	if($_SESSION['lang']== 'geo'){
		$lang = 'geo';
	}
   if($_SESSION['lang'] == 'eng'){
	$lang = 'eng';
	}
}
if(isset($_GET['lang'])){
	if($_GET['lang'] == 'geo'){
		$lang = 'geo';
		$_SESSION['lang']='geo';
	}	
	if($_GET['lang'] == 'eng'){
		$lang = 'eng';
		$_SESSION['lang']='eng';
	}
}
if(isset($lang)){
	if($lang!='geo' && $lang!='eng'){
		$lang='eng';
		$_SESSION['lang']='eng';
	}
}
else{
	$lang='geo';
	$_SESSION['lang']='geo';
}
////////////
$ie=false;
$ie55=false;
$ie6=false;
$ie7=false;
$ie8=false;
$firefox=false;
$opera=false;
$chrome=false;
////////////

if(!isset($_GET['action'])){
	$action='0';
}
else{
	$action=$_GET['action'];
}

if(!isset($_GET['video_id'])){
	$video_id='0';
}
else{
	$video_id=$_GET['video_id'];
}

if(!isset($_GET['pageon']) or (""==$_GET['pageon']) or !(is_numeric($_GET['pageon']))){
	$_GET['pageon']=0;
	$pageon=0;
}
else{
	$pageon=$_GET['pageon'];
}// else if isset pageon

if("page"==$action){
	if(!isset($_GET['page_id'])){
		$action='404';
	}
	else{
		if(is_numeric($_GET['page_id'])){
			$page_id=$_GET['page_id'];
		}
		else{
			$action='404';
		}
	}
}//if page == $action

if("nodes"==$action | "list"==$action){
	if(!isset($_GET['node_id']) && !isset($_GET['node_pid'])){
		$action='404';
	}
	else{
		if( isset($_GET['node_id']) ){
			if( is_numeric($_GET['node_id']) ){
				$node_id=$_GET['node_id'];
			}
			else{
				$action="404";
			}
		}
		//
		if( isset($_GET['node_pid']) ){
			if( is_numeric($_GET['node_pid']) ){
				$node_pid=$_GET['node_pid'];
			}
			else{
				$action="404";
			}
		}
	}
	//end critical part
	
	
	if(!isset($_GET['page_num']) ){//short circuit shemocmeba php-s ar gaugia?
		$page_num=0;
	}
	else{
		if( !($_GET['page_num']>0) ){
			$page_num=0;
		}
	}
}//if nodes == $action

if("search"==$action){
	if(!isset($_POST['search_keywords'])){
		$action='404';
	}
	else{
		$search_keywords=$_POST['search_keywords'];
		$search_keywords=str_replace(array("<",">","%","{","}"),"",$search_keywords);
	}
	//end critical part
	
}//if nodes == $action


///////////////////////////////////////////
switch($action){
	case "page":
		$idname="page_id";
		$idvar=$page_id;
	break;
	case "news":
		$idname="news_id";
		$idvar=$news_id;
	break;
	case "rest_menu":
		$idname="restaurant";
		$idvar=$restaurant;
	break;
	case "nodes":
		if(isset($node_id)){
			$idname="node_id";
			$idvar=$node_id;
		}
		if(isset($node_pid)){
			$idname="node_pid";
			$idvar=$node_pid;
		}
		break;
	default:
	$idname="search";
	$idvar="";
}
$gadabma_href_geo=generate_href("action",$action,$idname,$idvar,"pageon",$pageon,"lang","geo");
$gadabma_href_eng=generate_href("action",$action,$idname,$idvar,"pageon",$pageon,"lang","eng");

$langqs=array("&lang=geo","&lang=eng","&lang=ru","&lang=fra","lang=geo","lang=eng","lang=ru","lang=fra");//language query strings
//$langqs - shi tanmimdevroba aucilebelia ro jer &-ebiani switch-ebi chaanacvlos
$gadabma_href_geo="?".str_replace($langqs,"",$_SERVER['QUERY_STRING'])."&amp;lang=geo";
$gadabma_href_eng="?".str_replace($langqs,"",$_SERVER['QUERY_STRING'])."&amp;lang=eng";

$gadabma_href_geo=preg_replace("/\&(?!amp;)/","&amp;",$gadabma_href_geo);
$gadabma_href_eng=preg_replace("/\&(?!amp;)/","&amp;",$gadabma_href_eng);
//gadabmas ar itvaliscinebs, gaitvaliscinos da egaa, node-da newsebs id erti aqvt
//if action = page gadabma href = action = get gadabma...
if("page"==$action){
	$gadabma_href_geo="?action=page&amp;page_id=".get_gadabma($page_id,"geo")."&amp;pageon=".get_gadabma($page_id,"geo")."&amp;lang=geo";
	$gadabma_href_eng="?action=page&amp;page_id=".get_gadabma($page_id,"eng")."&amp;pageon=".get_gadabma($page_id,"eng")."&amp;lang=eng";
	
	//socar
	if("45"==$page_id or "46"==$page_id){
	$gadabma_href_geo="?action=page&amp;page_id=45&amp;pageon=45&amp;lang=geo";
	$gadabma_href_eng="?action=page&amp;page_id=46&amp;pageon=46&amp;lang=eng";
	}
	//socar
}


?>