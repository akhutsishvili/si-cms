<?
function get_template_entity($entity_name,$template_str){//roca dalagdeba CMS mere sheileba templates path-ic gadaeces
	
	$entstart_len=strlen("{".$entity_name.":begin}");
	$entity_len=strpos($template_str,"{".$entity_name.":end}")-strpos($template_str,"{".$entity_name.":begin}")-$entstart_len;
	
	return substr($template_str,strpos($template_str,"{".$entity_name.":begin}")+$entstart_len,$entity_len);
}
function template_replace_entity($entity_name,$self_closing,$inserting_str,$template_str){
	//replace(what,is self-closing tag, with what, in which string)
	if("true"==$self_closing){
		return str_replace("{$entity_name}",$inserting_str,$template_str);
	}
	else{
		$startindex=strpos($template_str,"{".$entity_name.":begin}");
		$part_before=substr($template_str,0,$startindex);
		
		$endindex=strpos($template_str,"{".$entity_name.":end}")+strlen("{".$entity_name.":end}");
		$part_after=substr($template_str,$endindex);
		
		return $part_before.$inserting_str.$part_after;
	}
}
?>