<?
class Sprite{
	public $name="";
	public $x=0;
	public $y=0;	
	public $w=1;
	public $h=1;
	function __construct($spr_name,$spr_x,$spr_y,$spr_w,$spr_h){
		$this->name=$spr_name;
		$this->x=$spr_x;
		$this->y=$spr_y;
		$this->w=$spr_w;
		$this->h=$spr_h;
	}
}
function sprite_icon($sprite_name){
	$predef_sprites["logo"]=new Sprite("logo",0,0,76,92);
	$predef_sprites["developed_by"]=new Sprite("developed_by",76,0,138,19);
	
	$predef_sprites["search_bg"]=new Sprite("search_bg",76,19,138,22);//unused
	$predef_sprites["search_btn_eng"]=new Sprite("search_btn_eng",76,41,50,10);
	$predef_sprites["search_btn_geo"]=new Sprite("search_btn_geo",76,51,41,11);
	
	$predef_sprites["lang_selected"]=new Sprite("lang_selected",76,62,5,8);
	$predef_sprites["lang_geo"]=new Sprite("lang_geo",76,70,24,8);
	$predef_sprites["lang_eng"]=new Sprite("lang_eng",76,78,22,8);
	
	$predef_sprites["lang_sel_geo"]=new Sprite("lang_sel_geo",138,41,24,8);
	$predef_sprites["lang_sel_eng"]=new Sprite("lang_sel_eng",138,49,23,8);
	
	$predef_sprites["statistic_geo"]=new Sprite("statistic_geo",0,92,231,19);
	$predef_sprites["statistic_eng"]=new Sprite("statistic_eng",0,111,231,19);
	
	$predef_sprites["more_geo"]=new Sprite("more_geo",214,0,59,22);
	$predef_sprites["more_eng"]=new Sprite("more_eng",214,22,40,15);
	
	$predef_sprites["fp_more_geo"]=new Sprite("fp_more_geo",273,0,58,23);
	$predef_sprites["fp_more_eng"]=new Sprite("fp_more_eng",273,23,43,20);
	
	$predef_sprites["send_geo"]=new Sprite("send_geo",331,0,49,19);
	$predef_sprites["send_eng"]=new Sprite("send_eng",331,19,49,19);
	
	$predef_sprites["our_address_geo"]=new Sprite("our_address_geo",0,130,168,14);
	$predef_sprites["contact_us_geo"]=new Sprite("contact_us_geo",0,144,174,14);
	$predef_sprites["contact_icons"]=new Sprite("contact_icons",380,0,16,105);
	
	return " background-image:url(pics/sprite_icons.png); background-position:-".$predef_sprites[$sprite_name]->x."px -".$predef_sprites[$sprite_name]->y."px; width:".$predef_sprites[$sprite_name]->w."px; height:".$predef_sprites[$sprite_name]->h."px; ";
}

?>